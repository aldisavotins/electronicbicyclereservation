package com.project.Autentica.demo.Controllers;

import com.project.Autentica.demo.Models.Bicycle;
import com.project.Autentica.demo.Models.BicycleReservation;
import com.project.Autentica.demo.Repositories.BicycleRepo;
import com.project.Autentica.demo.Repositories.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class BicycleController {

    @Autowired
    private BicycleRepo bicycleRepo;

    @Autowired
    private ReservationRepo reservationRepo;

//šī metode pievieno jaunu velosipēdu un padod skatam visus velosipēdus un rezervācijas
//lai pēc skata atjaunošanās nepazustu velosipēdu tabula
    @PostMapping("/createNewBicycle")
    public String createNewBicycle(Bicycle bicycle, Model model){
        bicycleRepo.save(bicycle);
        model.addAttribute("newBicycle", new Bicycle());
        List<Bicycle> bicycleList = bicycleRepo.findAll();
        model.addAttribute("newBicycleList",bicycleList);
        List<BicycleReservation> reservationList = reservationRepo.findAll();
        model.addAttribute("reservationList", reservationList);
        return "bicycles.html";
    }

}
