package com.project.Autentica.demo.Controllers;

import com.project.Autentica.demo.Models.Bicycle;
import com.project.Autentica.demo.Models.BicycleReservation;
import com.project.Autentica.demo.Models.User;
import com.project.Autentica.demo.Repositories.BicycleRepo;
import com.project.Autentica.demo.Repositories.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class WebController {

    @Autowired
    private BicycleRepo bicycleRepo;

    @Autowired
    private ReservationRepo reservationRepo;

    //šī metode padod reģistrācijas skatu ar jaunu lietotāja objektu
    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("newUser", new User());
        return "register";
    }

//    šī metode atgriež login skatu
    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    //šī metode atbild par galveno skatu. tā padod visus nepieciešamos mainīgos, lai skats darbotos korekti
    @GetMapping("/bicycles")
    public String getMainView(Model model){
        model.addAttribute("newBicycle", new Bicycle());
        List<Bicycle> bicycleList = bicycleRepo.findAll();
        model.addAttribute("newBicycleList",bicycleList);
        List<BicycleReservation> reservationList = reservationRepo.findAll();
        model.addAttribute("reservationList", reservationList);
        return "bicycles";
    }

    //šī metode atbild par izlogošanos no lapas
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }


}
