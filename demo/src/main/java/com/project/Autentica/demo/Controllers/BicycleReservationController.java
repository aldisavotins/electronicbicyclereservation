package com.project.Autentica.demo.Controllers;

import com.project.Autentica.demo.Models.Bicycle;
import com.project.Autentica.demo.Models.BicycleReservation;
import com.project.Autentica.demo.Models.User;
import com.project.Autentica.demo.Repositories.BicycleRepo;
import com.project.Autentica.demo.Repositories.ReservationRepo;
import com.project.Autentica.demo.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class BicycleReservationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationRepo reservationRepo;

    @Autowired
    private BicycleRepo bicycleRepo;


//šī metode saglabā rezervāciju un pievieno attiecīgos mainīgos, lai korekti strādātu galvenais skats
    @PostMapping("/createNewReservation")
    public String createNewReservation(Model model, BicycleReservation reservation) {
        reservationRepo.save(reservation);
        model.addAttribute("newBicycle", new Bicycle());
        List<Bicycle> bicycleList = bicycleRepo.findAll();
        model.addAttribute("newBicycleList", bicycleList);
        List<BicycleReservation> reservationList = reservationRepo.findAll();
        model.addAttribute("reservationList", reservationList);
        return "bicycles";
    }

    // šī metode padod rezervācijas skatu ar attiecīgajiem objektiem, kuri nepieciešami skata korektai uzrādīšanai
    @GetMapping("/createReservation")
    public String getReservationView(Model model) {
        BicycleReservation reservation = new BicycleReservation();
        List<Bicycle> availableBicycles = bicycleRepo.findAll();
        User currentUser = userService.getCurrentUser();

        model.addAttribute("newReservation", reservation);
        model.addAttribute("listOfBicycles", availableBicycles);
        model.addAttribute("currentUser", currentUser);

        return "createReservation";
    }

}

