package com.project.Autentica.demo.Models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "BicycleReservation")
public class BicycleReservation implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bicycle_id", nullable = false)
    private Bicycle bicycle;

    @Column(name = "timeFrom", nullable = false)
    private String timeFrom;

    @Column(name = "timeTo", nullable = false)
    private String timeTo;

    public BicycleReservation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bicycle getBicycle() {
        return bicycle;
    }

    public void setBicycle(Bicycle bicycle) {
        this.bicycle = bicycle;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public String toString() {
        return "BicycleReservation: " +
                "id= " + id +
                ", user= " + user +
                ", bicycle= " + bicycle +
                ", timeFrom= " + timeFrom +
                ", timeTo= " + timeTo;
    }
}
