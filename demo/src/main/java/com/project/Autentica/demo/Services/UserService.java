package com.project.Autentica.demo.Services;

import com.project.Autentica.demo.Models.User;
import com.project.Autentica.demo.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    //    Šī servisa mēŗkis ir padot pašreizējo lietotāju kontrolierim, lai pēc tam tas skatam varētu padot
//    lietotāja datus
    public User getCurrentUser() {
        String username = "";
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        List<User> userList = userRepo.findAll();
        User currentUser = new User();
        for (User u : userList) {
            if (u.getUsername().equals(username)) {
                currentUser = u;
            }
        }
        return currentUser;
    }

}
