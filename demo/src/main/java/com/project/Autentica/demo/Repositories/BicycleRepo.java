package com.project.Autentica.demo.Repositories;

import com.project.Autentica.demo.Models.Bicycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BicycleRepo extends JpaRepository<Bicycle, Integer> {
}
