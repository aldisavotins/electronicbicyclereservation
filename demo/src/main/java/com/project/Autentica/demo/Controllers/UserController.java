package com.project.Autentica.demo.Controllers;

import com.project.Autentica.demo.Models.User;
import com.project.Autentica.demo.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    @Autowired
    private UserRepo userRepo;

//    Šī metode saglabā jaunu lietotāju
    @PostMapping("/createNewUser")
    public String createNewUser(User user, Model model){
        userRepo.save(user);
        return "registrationComplete";
    }
}
