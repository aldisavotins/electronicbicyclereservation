package com.project.Autentica.demo.Models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Bicycle")
public class Bicycle implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private int id;

    @Column(name = "model")
    private String model;

    @Column(name = "serialNumber")
    private String serialNumber;

    @OneToMany(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private List<BicycleReservation> bicycleReservation;

    public Bicycle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<BicycleReservation> getBicycleReservation() {
        return bicycleReservation;
    }

    public void setBicycleReservation(List<BicycleReservation> bicycleReservation) {
        this.bicycleReservation = bicycleReservation;
    }

    @Override
    public String toString() {
        return "Bicycle: id = " + id +
                ", model = " + model +
                ", serialNumber = " + serialNumber +
                ", bicycleReservation = " + bicycleReservation;
    }
}
