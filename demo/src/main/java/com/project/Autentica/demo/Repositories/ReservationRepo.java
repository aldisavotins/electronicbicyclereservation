package com.project.Autentica.demo.Repositories;

import com.project.Autentica.demo.Models.BicycleReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepo extends JpaRepository<BicycleReservation, Integer> {
}
